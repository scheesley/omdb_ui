Search Results:-
 - Column Headings dont align with Results
 - Add Pageing [DONE]
 - Add a Sort, at the very least A-Z & Year [Sorting is out of the question as the API does not support it.
 - Allow Click on Picture too
 - Genre should be in results
	- Genre is not in the returned results - added type instead and decorated with graphics.
 - 'Year' is that released or made? [DONE] - Clarified
 - The last 'found' result always seems to be at the bottom, is that deliberate?
	- Not sure what is meant by this, need more detail.

Result Page:-
Need more space between Title and Blurb, at the very least compact it so its not soo wide to read - DONE

Fix error with missing default poster.

No results page - Where is it!! hehe