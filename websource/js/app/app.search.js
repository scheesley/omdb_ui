var omdbSearchApp = angular.module('OmdbApp', ['ngResource', 'omdb.rest.factory']);

omdbSearchApp.controller('OmdbSearchCtrl', function ($scope, OmdbWebService) {
  $scope.Math = window.Math;	
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  
  $scope.searched = false;
  $scope.detailed = false;
	
  $scope.searchFilm = function(resetPages) {
	if (resetPages) {
			$scope.currentPage = 1;	
		}
	OmdbWebService.search({s: $scope.name, page: $scope.currentPage}, function (dataResponse) {
		/* Success */		
		$scope.searchResult = dataResponse.Search;
		$scope.totalNoOfResults = parseInt(dataResponse.totalResults);
		$scope.numberOfPages=function(){
			return Math.ceil($scope.totalNoOfResults/$scope.pageSize);                
		}
		$scope.searched = true;
	}, function() {
		/* Fail */
		alert("Unable to search for film!");
	});
  };
  
  /* Increment the page number by 1 and pull the next page of results. */
  $scope.nextPage = function() {
	$scope.currentPage = $scope.currentPage + 1;
	$scope.searchFilm();
  };
  
  /* Decrement the page number by 1 and pull the previous page of results. */
  $scope.prevPage = function() {
	$scope.currentPage = $scope.currentPage - 1;
	$scope.searchFilm();
  };
  
  $scope.getFilmDetail = function(imdbID) {
	OmdbWebService.detail({i: imdbID}, function (dataResponse) {
		/* Success */
		$scope.searchDetail = dataResponse;
		$scope.detailed = true;
		console.log("dataResponse:" + JSON.stringify($scope.searchDetail));
	}, function() {
		/* Fail */
		alert("Unable to get details for film [ID:" + imdbID + "]");
	});
  };
});

/* A filter that allows our paginated list to work */
omdbSearchApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});