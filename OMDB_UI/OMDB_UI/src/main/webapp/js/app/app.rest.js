/* Create a service that will retrieve data from OMDB */
var rest = angular.module('omdb.rest.factory', ['ngResource']);

rest.factory("OmdbWebService", function ($resource) {
  var omdbWebService = $resource(endpointConfig.omdbServiceEndpoint,
          {function: '@function'},
  {'search': {method: 'GET', params: {s: '@s', page: '@page'}},
  'detail': {method: 'GET', params: {i: '@i'}}});

  return omdbWebService;
});